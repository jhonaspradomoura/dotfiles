set nocompatible
set guicursor=          		" Fix weird characters bug

set showmatch			    	" Show matching brackets
set number			        	" Show line numbers
set formatoptions+=o	    	" Continue comment markers in new lines
set noexpandtab			    	" Don't insert spaces when TAB is pressed
set tabstop=4			    	" Number of spaces used on TAB

set splitbelow			        " Horizontal split below current
set splitright			        " Vertical split to right of current

set nostartofline		        " Do not jump to first character with page commands

set textwidth=100               " Maximum text width before wrapping

set autoindent                  " Use same indenting on new lines
set smartindent                 " Smart autoindenting on new lines


set ignorecase                  " Search ignoring case
set smartcase                   " Keep case when searching with *
set incsearch                   " Incrementally search for matches as you type
set hlsearch                    " Highlight search results
set wrapscan                    " Searches wrap the end of the file

set backspace=indent,eol,start  " Intuitive backspacing in insert mode
set switchbuf=useopen,usetab    " Jump to the first open window in any tab
set showfulltag                 " Show tag and tidy search in completion
set complete=.                  " No wins, buffs, tags, include scanning
set completeopt=menuone         " Show menu even for one item
set completeopt+=noselect       " Do not select a match in the menu

let mapleader=","

if !&scrolloff
        set scrolloff=3	        " Show next 3 lines while scrolling.
endif
if !&sidescrolloff
		set sidescrolloff=5	    " Show next 5 columns while side-scrolling.
endif

set rnu							" Show line number as relative numbers
" set nornu

" Toggle between normal and relative numbering.
nnoremap <leader>r :call NumberToggle()<cr>

" Use deoplete.
let g:deoplete#enable_at_startup = 1

" Change shortcuts to CtrlP
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

syntax on

source $HOME/dotfiles/.config/nvim/plugins.vim
