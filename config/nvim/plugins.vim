if empty(glob('~/.vim/autoload/plug.vim'))
		silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
		autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.local/share/nvim/plugged')
		Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' },
		Plug 'arakashic/chromatica.nvim',
		Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }}
		Plug 'vim-airline/vim-airline'
		Plug 'ctrlpvim/ctrlp.vim'
		Plug 'vim-syntastic/syntastic'
		Plug 'junegunn/fzf.vim'
		Plug 'itchyny/lightline.vim'
		Plug 'terryma/vim-multiple-cursors'
		Plug 'tpope/vim-surround'
		Plug 'scrooloose/nerdtree'
		Plug 'editorconfig/editorconfig-vim'
		Plug 'w0rp/ale'
		Plug 'airblade/vim-gitgutter'

call plug#end()



