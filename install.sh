#!/bin/bash
sudo -v

zsh_custom="$HOME/.oh-my-zsh/custom"

install_zsh(){
	if [ $(which zsh) != "/usr/bin/zsh" ]
	then
		sudo apt install zsh
		sudo apt install curl git
		sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
		chsh -s $(which zsh)
	fi
	
	read -p "Do you want to install PowerLevel9k? (Y/n)" ans
	if [ "$ans" = "n" -o "$answer" = "N" ]
	then
		echo "PowerLevel9k wont be installed."
	else
		git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k 2>/dev/null
		read -p "Do you want to install Nerf Fonts? It's a 3GB download. (Y/n)" ans2
		if [ "$ans2" = "n" -o "$ans2" = "N" ]
		then
			echo "Nerd fonts wont be installed."
		else
			cd 
			git clone https://github.com/ryanoasis/nerd-fonts.git
			cd ~/nerd-fonts
			./install.sh DejaVuSansMono
		fi	
	fi
	

	echo "Instalando autojump"
	git clone git://github.com/wting/autojump.git $zsh_custom/plugins/autojump 2>/dev/null
	./$zsh_custom/plugins/autojump/install.py
	echo "Instalando syntax highlighting"
	git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $zsh_custom/plugins/zsh-syntax-highlighting 2>/dev/null
	echo "Instalando zsh autosuggestions"
	git clone https://github.com/zsh-users/zsh-autosuggestions $zsh_custom/plugins/zsh-autosuggestions 2>/dev/null
	echo "Instalando cd-reminder"
	git clone https://github.com/bartboy011/cd-reminder.git $zsh_custom/plugins/cd-reminder 2>/dev/null
	echo "Instalando auto update"
	git clone https://github.com/TamCore/autoupdate-oh-my-zsh-plugins $zsh_custom/plugins/autoupdate 2>/dev/null
	echo "Instalando diractions"
	git clone https://github.com/AdrieanKhisbe/diractions.git $zsh_custom/plugins/diractions 2>/dev/null
	echo "Instalando autoswitch_virtualenv"
	git clone https://github.com/MichaelAquilina/zsh-autoswitch-virtualenv.git $zsh_custom/plugins/autoswitch_virtualenv 2>/dev/null
	
    echo "Instalando you-should-use"
	git clone https://github.com/MichaelAquilina/zsh-you-should-use.git
	$zsh_custom/plugins/you-should-use 2>/dev/null
       
	echo "Linkando o .zshrc que esta no repositorio ao arquivo na home"
	sudo rm ~/.zshrc ~/.zshrc_local
	sudo ln -srf $(pwd)/.config/zsh/.zshrc ~
	sudo ln -srf $(pwd)/.config/zsh/.zshrc_local ~
}


install_nvim()
{
	echo "Installing neovim.."
	sudo apt update
	sudo apt install neovim
	sudo apt-get install python-neovim python3-neovim
	mkdir -p $HOME/.config/nvim/
	ln -srf .config/nvim/init.vim $HOME/.config/nvim/
	ln -srf .config/nvim/plugins.vim $HOME/.config.nvim
	export EDITOR="nvim"
}

install_vim()
{
	echo "Installing vim.."
	sudo add-apt-repository ppa:jonathonf/vim
	sudo apt update
	sudo apt install vim
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	sudo ln -sr $HOME/dotfiles/.config/.vimrc $HOME
	sudo ln -sr $HOME/dotfiles/.config/.vimrc.plug $HOME
    

}

install_alacritty()
{
    echo "Installing terminal emulator alacritty.."
    sudo add-apt-repository ppa:mmstick76/alacritty
    sudo apt install alacritty
    ln -sr .config/alacrity.yml $HOME
}

read -p "Do you want to install zsh? (Y/n)" ans
[[ $ans = "n" || $ans = "N" ]] && echo "answer is no" || install_zsh

read -p "Do you want to install vim or neovim? (Y/n)" ans
[[ $ans = "n" || $ans = "N" ]] || read -p "Which one? (v/n)" vim_ans
case $vim_ans in
    "v" | "V") install_vim;;
    "n" | "N") install_nvim;;
esac

read -p "Do you want to install alacritty? (Y/n)" ans
[[ $ans != "n" || $ans != "N" ]] || install_alacritty

